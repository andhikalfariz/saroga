from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def tentang(request):
    return render(request, 'home/tentang.html')

def needfindings(request):
    return render(request, 'home/needfindings.html')

def pov(request):
    return render(request, 'home/pov.html')

def conceptvideo(request):
    return render(request, 'home/conceptvideo.html')

def lowfi(request):
    return render(request, 'home/lowfi.html')

def medfi(request):
    return render(request, 'home/medfi.html')

def highfi(request):
    return render(request, 'home/highfi.html')

def heuristic(request):
    return render(request, 'home/heuristic.html')

def posterpitch(request):
    return render(request, 'home/posterpitch.html')