from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('tentang/', views.tentang, name='tentang'),
    path('needfindings/', views.needfindings, name='needfindings'),
    path('pov/', views.pov, name='pov'),
    path('conceptvideo/', views.conceptvideo, name='conceptvideo'),
    path('lowfi/', views.lowfi, name='lowfi'),
    path('medfi/', views.medfi, name='medfi'),
    path('highfi/', views.highfi, name='highfi'),
    path('heuristic/', views.heuristic, name='heuristic'),
    path('posterpitch/', views.posterpitch, name='posterpitch'),
]